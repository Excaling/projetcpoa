package ordinateur.operationComposants;

import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouterRAM;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirerRAM;

public class OperationRAM extends OperationComposant {
	
	public OperationRAM(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM(); 
		this.addComposant = new ComportementAjouterRAM(this.pc);
		this.removeComposant = new ComportementRetirerRAM(this.pc);
	}
	
	@Override
	public void echangerComposant(Composant pComposant) {
		// TODO Auto-generated method stub
		this.removeComposant.retirerComposant();
		try {
			this.addComposant.ajouterComposant(pComposant);
		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

}
