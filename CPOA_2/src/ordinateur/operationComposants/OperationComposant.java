package ordinateur.operationComposants;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouter;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirer;
/**
 * Classe abstraite permettant les echanges de composants du pc lorsqu'il y a surchauffe
 * @author Jonathan
 *
 */
public abstract class OperationComposant {
	protected DELLX4000 pc;
	protected CarteMere cm;
	protected ComportementRetirer removeComposant;
	protected ComportementAjouter addComposant;
	/**
	 * methode permettant d'echanger un composant en surchauffe avec un autre composant
	 * @param pComposant
	 */
	public abstract void echangerComposant(Composant pComposant);
	/**
	 * 
	 * Methode permettant de recuperer le comportement d'extraction de composant
	 * @return removeComposant
	 */
	public ComportementRetirer getComportementRetirer() {
		return this.removeComposant;
	}
	/**
	 * Methode permettant de recuperer le comportement d'ajout d'un composant.
	 * @return addComposant
	 */
	public ComportementAjouter getComportementAjouter() {
		return this.addComposant;
	}
}
