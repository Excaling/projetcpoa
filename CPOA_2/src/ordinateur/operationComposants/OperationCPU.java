package ordinateur.operationComposants;

import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouter;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouterCPU;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirer;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirerCPU;

public class OperationCPU extends OperationComposant {
	
	public OperationCPU(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = pPC.getCM();
		this.removeComposant = new ComportementRetirerCPU(this.pc);
		this.addComposant = new ComportementAjouterCPU(this.pc);
	}
	@Override
	public void echangerComposant(Composant pComposant)   {
		// TODO Auto-generated method stub
		this.removeComposant.retirerComposant();
		try {
			this.addComposant.ajouterComposant(pComposant);	
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	

}
