package ordinateur.composants;
import etat.*;
import ordinateur.composants.comportements.comportementAllumerEteindre.*;
/**
 * 
 * Classe abstraite des composant
 * 
 * @author Jonathan, Tommy
 *
 */
public abstract class Composant {
	
	protected Etat etat; 
    protected AllumerEteindre allumerEteindre;
    protected String frequence;
	public abstract String getFrequence();
	/**
	 * Methode setFrequence qui permet de parametrer la frequence d'un composant.
	 * 
	 * @param frequence
	 */
	public abstract void setFrequence(String frequence);
	/**
	 * Methode permettant d'obtenir l'etat du composant
	 * 
	 * @return Etat
	 */
	public AllumerEteindre getAllumerEteindre() {
		return this.allumerEteindre;
	}
	
}
