package ordinateur.composants.comportements.comportementAjouterComposant;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.Processeur;

public class ComportementAjouterCPU implements ComportementAjouter{

	private DELLX4000 pc;
	private CarteMere cm;
	
	public ComportementAjouterCPU(DELLX4000 pPC) {
		this.pc = pPC ;
		this.cm = pPC.getCM();
	}

	@Override
	public void ajouterComposant(Composant pCompo) throws Exception{

		if(!(pCompo instanceof Processeur)) {
			throw new Exception("Veuillez fournir un Processeur");
		}
		if(pc.getCPU() != null && cm.getCPU() != null) {
			System.out.println("Veuillez Retirer le composant dej� existant");
		}else {
			this.pc.setCPU((Processeur) pCompo);
			this.cm.setCPU((Processeur) pCompo);
			pc.AllumerPC();
			System.out.println("Ajout d'un processeur");
		}
	}

	
}
