package ordinateur.composants.comportements.comportementAjouterComposant;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.DisqueDur;

public class ComportementAjouterHDD implements ComportementAjouter {
	
	private DELLX4000 pc;
	private CarteMere cm;
	
	public ComportementAjouterHDD(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM();
	}
	
	@Override
	public void ajouterComposant(Composant pCompo) throws Exception {
		// TODO Auto-generated method stub
		if(!pCompo.getClass().getName().equals("DisqueDur")) {
			throw new Exception("Veuillez fournir un disque dur");
		}
		if(this.pc.getHDDList().size() == 2 && this.cm.getHDDList().size() == 2 ) {
			System.out.println("Veuillez Retirer un disquedur dej� ");
		}
		else {
			this.pc.getHDDList().add((DisqueDur) pCompo);
			this.cm.getHDDList().add((DisqueDur)pCompo);
			System.out.println("Ajout d'un d'un disque dur");
		}
	}

}
