package ordinateur.composants.comportements.comportementAjouterComposant;

import ordinateur.composants.Composant;
/**
 * Interface des ajouts de composants dans le pc
 * @author Jonathan, Tommy
 *
 */
public interface ComportementAjouter {
	/**
	 * Methode permettant d'ajouter un composant dans la carte mere et le pc.
	 * Retourne une exception si le composant n'est pas le bons
	 * @param pCompo
	 * @throws Exception
	 */
	public void ajouterComposant(Composant pCompo)throws Exception;
}
