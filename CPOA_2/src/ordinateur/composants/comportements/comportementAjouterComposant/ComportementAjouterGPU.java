package ordinateur.composants.comportements.comportementAjouterComposant;

import ordinateur.composants.CarteGraphique;
import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.Processeur;

public class ComportementAjouterGPU implements ComportementAjouter {
	private DELLX4000 pc;
	private CarteMere cm; 
	
	public ComportementAjouterGPU(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM();
	}

	@Override
	public void ajouterComposant(Composant pCompo) throws Exception {
		if(!(pCompo instanceof  CarteGraphique)) {
			throw new Exception("Veuillez fournir une carte graphique");
		}
		if(pc.getGPU() != null && cm.getGPU() != null) {
			System.out.println("Veuillez Retirer le composant dej� existant");
		}else {
			this.pc.setGPU((CarteGraphique) pCompo);
			this.cm.setGPU((CarteGraphique) pCompo);
			pc.AllumerPC();
			System.out.println("Ajout d'une carte graphique");
		}
		
	}
	
	
}
