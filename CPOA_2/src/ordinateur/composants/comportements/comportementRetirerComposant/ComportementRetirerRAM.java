package ordinateur.composants.comportements.comportementRetirerComposant;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;

public class ComportementRetirerRAM implements ComportementRetirer{
	
	private DELLX4000 pc;
	private CarteMere cm;
	
	public ComportementRetirerRAM(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM();
	}

	@Override
	public void retirerComposant() {
		String[] frequenceRAM = this.cm.getRAM().getFrequence().split("MHz");
		if(Float.parseFloat(frequenceRAM[0]) > 3000) {
			this.pc.EteindrePC();
			this.pc.setRAM(null);
			this.cm.setRAM(null);
			System.out.println("Retrait RAM");
		}
	}

	@Override
	public void retirerComposantChoix(Composant pCompo) {
		// TODO Auto-generated method stub
		System.out.print("Pas besoins de cette methode pour la ram");
	}

}
