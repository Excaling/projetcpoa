package ordinateur.composants.comportements.comportementRetirerComposant;

import ordinateur.composants.*;

public class ComportementRetirerGPU implements ComportementRetirer{
	
	private DELLX4000 pc; 
	private CarteMere cm;
	
	public ComportementRetirerGPU(DELLX4000 pPc) {
		this.pc = pPc;
		this.cm = this.pc.getCM();
	}
	
	@Override
	public void retirerComposant() {
		String[] frequenceGPU = this.pc.getGPU().getFrequence().split("MHz");
		if(Float.parseFloat(frequenceGPU[0]) > 1800) {
			this.pc.EteindrePC();
			this.cm.setCPU(null);
			this.pc.setGPU(null);
			System.out.println("retrait GPU");
		}else {
			System.out.println("Extraction impossible car non en chauffe");
		}
		
	}

	@Override
	public void retirerComposantChoix(Composant pCompo) {
		// TODO Auto-generated method stub
		System.out.println("Pas besoins de cette methode pour le GPU");
	}
	
}
