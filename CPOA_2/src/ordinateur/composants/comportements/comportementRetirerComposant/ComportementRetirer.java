package ordinateur.composants.comportements.comportementRetirerComposant;

import ordinateur.composants.Composant;
/**
 * 
 * Interface permettant de retirer des composants.
 * @author Jonathan
 *
 */
public interface ComportementRetirer {
	/**
	 * methode permettant de retirer un composant
	 */
	public void retirerComposant();
	/**
	 * methode de retirer un composant parmis un liste de composants de m�me type que ce composant
	 * @param pCompo
	 */
	public void retirerComposantChoix(Composant pCompo);
}
