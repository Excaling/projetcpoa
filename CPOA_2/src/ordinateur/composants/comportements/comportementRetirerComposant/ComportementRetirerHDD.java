package ordinateur.composants.comportements.comportementRetirerComposant;

import java.util.ArrayList;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.DisqueDur;

public class ComportementRetirerHDD implements ComportementRetirer{
	
	private DELLX4000 pc; 
	private CarteMere cm;
	
	public ComportementRetirerHDD (DELLX4000 pPC) {
		this.pc = pPC; 
		this.cm = this.pc.getCM();
	}

	@Override
	public void retirerComposant() {
		
		System.out.println("pas besoins de cette methode pour les disque dur");
	}

	@Override
	public void retirerComposantChoix(Composant pCompo) {
		// TODO Auto-generated method stub
		if(this.cm.getHDDList().size() <= 0 && this.pc.getHDDList().size() <= 0) {
			System.out.println("On ne peut pas retirer quand il n'y a pas de disque dur");
		}else {
			this.pc.EteindrePC();
			ArrayList<DisqueDur> HddPCList = this.pc.getHDDList();
			ArrayList<DisqueDur> HddCMList = this.cm.getHDDList();
			HddPCList.remove((DisqueDur) pCompo);
			HddCMList.remove((DisqueDur) pCompo);
			System.out.print("Retrait HDD");
		}
	}

}
