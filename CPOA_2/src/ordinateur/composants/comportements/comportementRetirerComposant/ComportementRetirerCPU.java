package ordinateur.composants.comportements.comportementRetirerComposant;

import etat.Etat;
import etat.Eteint;
import ordinateur.composants.*;

public class ComportementRetirerCPU implements ComportementRetirer{
	private DELLX4000 pc;
	private CarteMere cm;
	public ComportementRetirerCPU(DELLX4000 pPc) {
		this.pc = pPc;
		this.cm = this.pc.getCM();
	}
	
	@Override
	public void retirerComposant() {
		String frequenceCPU[] = this.cm.getCPU().getFrequence().split("GHz");
		if (Float.parseFloat(frequenceCPU[0])> 3) {
			this.pc.EteindrePC();
			this.pc.setCPU(null);
			this.cm.setCPU(null);
			System.out.println("retrait CPU");
		}else {
			System.out.println("pas de surchauffe donc inutile de retirer");
		}
	}

	@Override
	public void retirerComposantChoix(Composant pCompo) {
		
		System.out.println("Pas besoins pour le CPU");
		
	}
	
}
