package ordinateur.composants.comportements.comportementAllumerEteindre;

import etat.Allume;

import etat.Etat;
import etat.Eteint;

public class ComportementAllumerEteindreGPU implements AllumerEteindre {
	private Etat etat;
	
	public ComportementAllumerEteindreGPU(Etat pEtat) {
		this.etat = pEtat;
	}
	
	
	@Override
	public void setEtat(Etat pEtat) {
		// TODO Auto-generated method stub
		this.etat = pEtat;
		System.out.println("GPU: " + this.etat.toString());
	}
	@Override
	public Etat getEtat() throws Exception {
		if(this.etat == null) {
			throw new Exception("Veuillez Avoir un etat non null");
		}
		// TODO Auto-generated method stub
		return this.etat;
	}
	
}
