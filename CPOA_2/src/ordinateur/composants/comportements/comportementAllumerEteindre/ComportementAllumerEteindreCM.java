package ordinateur.composants.comportements.comportementAllumerEteindre;
import etat.Allume;
import etat.Etat;
import etat.Eteint;
public class ComportementAllumerEteindreCM implements AllumerEteindre{
	private Etat etat;
	
	public ComportementAllumerEteindreCM(Etat pEtat) {
		this.etat = pEtat;
	}
	

	@Override
	public void setEtat(Etat pEtat) {
		// TODO Auto-generated method stub
		this.etat = pEtat;
		System.out.print("Carte mere" + this.etat.toString());
		
	}

	@Override
	public Etat getEtat() throws Exception {
		if(this.etat == null) {
			throw new Exception("Veuillez Avoir un etat non null");
		}
		// TODO Auto-generated method stub
		return this.etat;
	}

	
	
}
