package ordinateur.composants;

import etat.Eteint;

import ordinateur.composants.comportements.comportementAllumerEteindre.ComportementAllumerEteindreProcesseur;
/**
 * Classe de Processeur
 * @author Jonathan, Tommy
 * 
 */
public class Processeur extends Composant{

    public String nomCPU;
/**
 * Constructeur parametre du processeur
 * @param nomCPU
 * @param pFrequence
 */
    public Processeur(String nomCPU, String pFrequence) {
        this.nomCPU = nomCPU;
        this.etat = null;
        this.allumerEteindre = new ComportementAllumerEteindreProcesseur(this.etat);
        this.frequence = pFrequence + "GHz";
    }

    
	@Override
	public String getFrequence() {
		// TODO Auto-generated method stub
		return this.frequence;
	}
	
	public String toString() {
		return " frequence: " +this.frequence+ " nomCPU: " + nomCPU;
	}

	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		
		this.frequence = frequence + "GHz";
		
	}
}
