import etat.Allume;
import etat.Etat;
import etat.Eteint;
import ordinateur.composants.CableSata;
import ordinateur.composants.CarteGraphique;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.DisqueDur;
import ordinateur.composants.Processeur;
import ordinateur.composants.RAM;
import ordinateur.operationComposants.OperationCPU;
import ordinateur.operationComposants.OperationComposant;
import ordinateur.operationComposants.OperationGPU;
import ordinateur.operationComposants.OperationRAM;

public class Main {
    public static void main(String[] args) {

       DELLX4000 MonOrdi = DELLX4000.getInstance();
        MonOrdi.AllumerPC();
        MonOrdi.EteindrePC();
        MonOrdi.getCM().getCPU().setFrequence("4.0");
        
        //Echanger composant CPU
        System.out.println("--------------------------");
        OperationComposant op = new OperationCPU(MonOrdi);
        System.out.println(MonOrdi.getCM().getCPU().toString());
        op.echangerComposant(new Processeur("i5 7th gen", "2.5"));
        System.out.println(MonOrdi.getCM().getCPU().toString());
        System.out.println("--------------------------");

        //Echanger composant GPU
        System.out.print(MonOrdi.getCM().getGPU().toString());

        MonOrdi.getCM().getGPU().setFrequence("3000MHz");;
        op = new OperationGPU(MonOrdi);
        op.echangerComposant(new CarteGraphique("GTX 1050", "1608MHz"));
        System.out.println(MonOrdi.getCM().getGPU().toString());
        System.out.println("--------------------------");

        //Echanger Composant RAM
        
        MonOrdi.getCM().getRAM().setFrequence("5000MHz");
        System.out.println(MonOrdi.getCM().getRAM().toString());
        op = new OperationRAM(MonOrdi);
        op.echangerComposant(new RAM("1500MHz"));
        System.out.println(MonOrdi.getCM().getRAM().toString());
        
    }
}
