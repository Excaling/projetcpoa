package etat;

import ordinateur.composants.Composant;



public class Allume implements Etat{
	

	@Override
	public void Actionner(Composant Compo) {
		Compo.getAllumerEteindre().setEtat(this);
	}
	public String toString() {
		return "Je m'allume";
	}
}
